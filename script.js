// console.log(`Hello world`); 

let trainer = {
	name: `Ash Ketchum`,
	age: 10,
	pokemon: [`Pickachu`, `Charizard`, `Squirtle`, `Bulbasaur`],
	friends: {
        Misty: [`Starmie`, `Psyduck` ],
        Brock: [`Geodude`, `Vulpix`],
        },
	talk: function() {
		console.log(`Pickachu! I choose you!`);
	}
}
console.log(trainer);

console.log(`Result of dot notation: `);
console.log(`${trainer.name}`);

console.log(`Result of square bracket notation: `);
console.log(trainer[`pokemon`]);

console.log(`Result of talk method: `);
trainer.talk();

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}`);

        target.health -= this.attack;
        console.log(`${target.name} health is now reduced to ${target.health}.`);


        if(target.health <= 0) {
            target.faint();
        }
    }
    this.faint = function() { 
        console.log(`${this.name} fainted`);
    }
}

let pikachu = new Pokemon(`Pikachu`, 12);
console.log(pikachu);

let geodude = new Pokemon(`Geodude`, 8);
console.log(geodude);

let mewtwo = new Pokemon(`Mewtwo`, 100);
console.log(mewtwo);

geodude.tackle(pikachu);

Pokemon.health = 16;
console.log(pikachu);

mewtwo.tackle(geodude);

Pokemon.health = -84;
console.log(geodude);




